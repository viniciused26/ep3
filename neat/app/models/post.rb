class Post < ApplicationRecord

  has_many :comments 

  mount_uploader :image, ImageUploader

  extend FriendlyId
  friendly_id :title, use: :slugged

  def should_generate_new_friendly_id?
    title_changed?
  end



end
